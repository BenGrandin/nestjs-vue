import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';

@Injectable()
export class AuthGuard implements CanActivate {
	constructor(private jwtService: JwtService) {
	}

	canActivate(context: ExecutionContext) {

		const request = context.switchToHttp().getRequest();
		const jwt = request.cookies['jwt'];

		try {
			if (this.jwtService.verify(jwt)) return true;
		} catch (e) {
			return false;
		}
	}
}