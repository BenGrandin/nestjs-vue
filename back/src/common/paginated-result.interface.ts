export class IPaginatedResult<T> {
	data: T[];
	meta: {
		total: number,
		page: number,
		last_page: number
	};
}