import { Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';
import { IPaginatedResult } from '../paginated-result.interface';

@Injectable()
export abstract class AbstractService<T> {

	protected constructor(
		protected readonly repository: Repository<T>,
	) {
	}

	async all(relations = []): Promise<T[]> {
		return await this.repository.find({ relations });
	}

	async paginate(page = 1, relations = []): Promise<IPaginatedResult<T>> {
		const take = 15;
		const [data, total] = await this.repository.findAndCount({
			take,
			skip: (page - 1) * take,
			relations,
		});

		return {
			data,
			meta: {
				total,
				page,
				last_page: Math.ceil(total / take),
			},
		};
	}

	async create(data): Promise<T> {
		return this.repository.save(data);
	}

	async findOne(condition, relations = []): Promise<T> {
		return this.repository.findOne(condition, { relations });
	}

	async update(id: number, data): Promise<any> {
		return this.repository.update(id, data);
	}

	async delete(id: number): Promise<any> {
		return this.repository.delete(id);
	}
}