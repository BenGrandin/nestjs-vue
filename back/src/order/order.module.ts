import { Module } from '@nestjs/common';
import { OrderController } from './order.controller';
import { OrderService } from './order.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CommonModule } from '../common/common.module';
import { Order } from './order.entity';
import { OrderItem } from './order-item.entity';

@Module({
	imports: [
		TypeOrmModule.forFeature([Order, OrderItem]),
		CommonModule,
	],
	controllers: [OrderController],
	providers: [OrderService],
})
export class OrderModule {
}