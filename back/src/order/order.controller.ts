import { ClassSerializerInterceptor, Controller, Get, Post, Query, Res, UseInterceptors } from '@nestjs/common';
import { OrderService } from './order.service';
import { Response } from 'express';
import { Parser } from 'json2csv';

// @UseGuards(AuthGuard)
@UseInterceptors(ClassSerializerInterceptor)
@Controller()
export class OrderController {
	constructor(private orderService: OrderService) {
	}

	@Get('orders')
	async all(@Query('page')page: number = 1) {
		// return this.orderService.all(['order_items']);
		return this.orderService.paginate(page, ['order_items']);
	}

	@Post('export')
	async export(@Res() res: Response) {
		const parser = new Parser({
			fields: ['ID', 'Name', 'Email', 'Product Title', 'Price', 'Quantity'],
		});

		let json = [];
		const orders = await this.orderService.all(['order_items']);
		orders.forEach(({ id, name, email, order_items }) => {
			json.push({
				'ID': id,
				'Name': name,
				'Email': email,
				'Product Title': '',
				'Price': '',
				'Quantity': '',
			});

			order_items.forEach(({ product_title, price, quantity }) => {
				json.push({
					'ID': '',
					'Name': '',
					'Email': '',
					'Product Title': product_title,
					'Price': price,
					'Quantity': quantity,
				});
			});
		});
		console.log(json);
		const csv = parser.parse(json);

		res.header('Content-type', 'text/csv');
		res.attachment('orders.csv');
		res.send(csv);
	}

	@Get('chart')
	async chart() {
		return this.orderService.chart();
	}
}